#include "Queu.h"

/******************************************************************/
//
//					CLASS	Queu
//
/******************************************************************/

void Queu::Initialize()
{
	Pointer=0;
	for(byte i=0;i<QueuSize;i++)
	{
		QueuArray[i]=0;
	}	
}

//Pointer always point to the empty element above the last ocuppied element
void Queu::Enqueu(long Item)
{
	if(Pointer+1<QueuSize)
	{
		QueuArray[Pointer]=Item;
		Pointer=Pointer+1;
	}
}

long Queu::Dequeu()
{	
	if(this->MessagesInQueu())
	{
		long Item=QueuArray[0];
		//Shift items down
		for(byte i=0;i<Pointer-1;i++)
		{
			QueuArray[i]=QueuArray[i+1];
		}
		Pointer=Pointer-1;
		return Item;
	}
	else
		return -1;
}

bool Queu::MessagesInQueu()
{
	if(Pointer>0)
		return TRUE;
	else
		return FALSE;
}

byte Queu::MessagesInBuffer()
{
	return Pointer;
}

long Queu::LastElement()
{
	return QueuArray[Pointer-1];
}

long Queu::FirstElement()
{
	return QueuArray[0];
}
