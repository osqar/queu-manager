#ifndef Queu_h
#define Queu_h

#include <Arduino.h>
#include <inttypes.h>

/*********************************************************
					Queu CLASS
*********************************************************/
#define QueuSize 20
#define TRUE 1
#define FALSE 0

class Queu
{
	public:
		void Initialize();
		void Enqueu(long);
		long Dequeu();
		long LastElement();
		long FirstElement();
		bool MessagesInQueu();
		byte MessagesInBuffer();
	private:
		byte Pointer;
		long QueuArray[QueuSize];
		long AuxiliarQueu;
};


#endif

